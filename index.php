<?php

// Task 1 Написать функцию trimText() которая обрезает текст до заданного количества слов – функция принимает текст и количество слов (wp_trim_words не использовать)
function trimText($text, $word_count)
{
    $words = preg_split('/\s+/', $text);
    $trimmed_words = array_slice($words, 0, $word_count);
    $trimmed_text = implode(' ', $trimmed_words);
    return $trimmed_text;
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $input_text = $_POST['text'];
    $word_count = $_POST['word_count'];

    $trimmed_text = trimText($input_text, $word_count);
}
?>
    <form method="post" action="">
        <label for="text">Text:</label><br>
        <input type="text" id="text" name="text" placeholder="please, input text" required>
        <label for="word_count">Word number: </label><br>
        <input type="number" id="word_count" name="word_count" required>
        <input type="submit" value="Trim your text">
    </form>

<?php if (isset($trimmed_text)) { ?>
    <h2>Result: </h2>
    <p><?php echo $trimmed_text; ?></p>
<?php } ?>

<?php
// Task 2 Написать функцию которая принимает дату публикации поста и выводит текст в формате “x days ago” – (если разница меньше дня – “x hours ago”)

function getPostTime()
{
$args = array(
'post_type' => 'post',
'orderby' => 'date',
'order' => 'DESC',
'posts_per_page' => 1
);
$query = new WP_Query($args);

if ($query->have_posts()) {
$query->the_post();
$post_date = get_the_date('Y-m-d H:i:s');
$current_date = current_time('Y-m-d H:i:s');
$post_date_obj = new DateTime($post_date);
$current_date_obj = new DateTime($current_date);
$interval = $current_date_obj->diff($post_date_obj);

if ($interval->days > 0) {
return $interval->format('%a days ago');
} else {
return $interval->format('%h hours ago');
}
}

return '';
}

$timestamp = getPostTime();
echo $timestamp;